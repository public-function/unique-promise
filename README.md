# Unique Promise

[![NPM Version][npm-version-image]][npm-url]
[![NPM Install Size][npm-install-size-image]][npm-install-size-url]

```ts
import uniquePromise from '@public-function/unqiue-promise';

```


[npm-url]: https://npmjs.org/package/@public-function/unique-promise
[npm-version-image]: https://badgen.net/npm/v/@public-function/unique-promise
[npm-install-size-image]: https://badgen.net/packagephobia/install/@public-function/unique-promise
[npm-install-size-url]: https://packagephobia.com/result?p=/@public-function/unique-promise
